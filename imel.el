;;; imel.el --- Imèl                   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Jaft

;; Author: Jaft <jaft.r@mail.mayfirst.org>
;; URL: https://codeberg.org/Jaft/Emacs-Im-l
;; Package-Requires: ((emacs "28.1") (async "20220928.1909"))
;; Version: 1.0
;; Keywords: mail

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Sterf

;;; Code:
(require 'async)
(require 'auth-source)
(require 'hierarchy)
(require 'imap)
(require 'subr-x)

(defconst imel----folders-buffer-name "Imel: Folders"
  "The name given to the buffer that displays E-mail folders.")

(defcustom imel---accounts-info '()
  "The E-mail accounts you want to have Imèl display.

Value should be in the form of:

'(account1 ((imap-server . \"imap.server.com\")
            (imap-port   . 143)
            (imap-stream . network)
            (imap-auth   . anonymous))
  account2 ((imap-server . \"imap.example.com\")
            (imap-port   . 993)
            (imap-stream . tls)
            (imap-auth   . login)))

If 'imap-port' is not provided, 'nil' will be passed to `imap-open' and default
to 143; you may want to set this to 993, instead. Check what your E-mail
provider uses for IMAP connections.

'imap-stream' is one of any valid default values of `imap-streams'. If not
provided, 'nil' will be passed to `imap-open' and prioritize the connections in
order of `imap-streams'.

'imap-auth' is one of any valid default values of `imap-authenticators'. If not
provided, 'nil' will be passed to `imap-open' and prioritize the authenticators
in order of `imap-authenticators'."
  :type  '(alist :value-type (alist :value-type (choice integer
                                                        string
                                                        symbol)))
  :group 'imel)

(defvar imel---account-keys-to-imap-buffers '()
  "An alist of the keys in `imel---accounts-info' to buffers used by `imap.el'.

`imap.el' generates a buffer upon `imap-open' that is then used for all future
IMAP operations, from checking if the connection is still open to basic commands
to the IMAP server.

As such, Imèl saves this buffer here to be able to fetch it when performing
operations on a particular account.")

(defun imel--get-imap-buffer (account-key)
  "Helper function to fetch IMAP connection buffers for particular E-mail
accounts.

ACCOUNT-KEY is a symbol set by the user in `imel---accounts-info' which
corresponds to a particular E-mail account."

  (alist-get account-key imel---account-keys-to-imap-buffers))

(define-derived-mode imel--folders-mode special-mode "Imel:Folders"
  "Major mode for listing E-mail account folders."

  (define-key imel--folders-mode-map    (kbd "n")         #'widget-forward)
  (define-key imel--folders-mode-map    (kbd "p")         #'widget-backward)
  (define-key tree-widget-button-keymap (kbd "TAB")       #'widget-button-press)
  (define-key tree-widget-button-keymap (kbd "<backtab>") nil)
  (define-key tree-widget-button-keymap (kbd "RET")       #'(lambda ()
                                                              (interactive)

                                                              (forward-button 1)

                                                              (let ((b (button-at (point))))
                                                                (if (string= (what-line) "Line 1")
                                                                    (move-beginning-of-line nil)
                                                                  (widget-backward 1))

                                                                (button-activate b)))))



;;;###autoload
(defun imel ()
  ""
  (interactive)

  (if (get-buffer imel----folders-buffer-name)
      (switch-to-buffer imel----folders-buffer-name)
    (let ((folder-hierarchy (hierarchy-new))
          (folder-buffer    (switch-to-buffer (generate-new-buffer
                                                imel----folders-buffer-name))))
      (insert "Loading folders; please wait…")
      (setq buffer-read-only t)

      (make-thread
        #'(lambda ()
            (dolist (account imel---accounts-info)
              (when-let* ((key         (car account))
                          (conn-buffer (imel--connect-or-reconnect key))
                          (hier-root   "Accounts"))
                (dolist (folder (imap-mailbox-list "*" nil t conn-buffer))
                  (let* ((account-name (symbol-name key))
                         (name-fn      (lambda (lst announce)
                                         (let ((name (car lst))
                                               (path (string-join (nreverse lst)
                                                                  "/")))
                                           (if (or (string-equal account-name name)
                                                   (string-equal hier-root    name))
                                               name
                                             (when announce
                                               (message "Fetching Mailbox info. of %s"
                                                        path))

                                             (let ((new (imap-mailbox-status path
                                                                             '(recent
                                                                               uidnext
                                                                               uidvalidity
                                                                               unseen)
                                                                             conn-buffer)))
                                               `((recent      . ,(car new))
                                                 (uidnext     . ,(cadr new))
                                                 (uidvalidity . ,(caddr new))
                                                 (unseen      . ,(cadddr new))
                                                 (name        . ,name)
                                                 (account     . ,key))))))))
                    (hierarchy-add-tree folder-hierarchy
                                        (funcall name-fn
                                                 ;; For reasons absolutely
                                                 ;; unbeknownst to me (dolist?),
                                                 ;; setting this in `let'
                                                 ;; causes the list to get
                                                 ;; truncated to a list of
                                                 ;; length one; so we have
                                                 ;; to run this in place,
                                                 ;; everywhere we need it
                                                 (nreverse (split-string folder
                                                                         "/"))
                                                 t)
                                        (lambda (item)
                                          (if-let ((parent (cadr
                                                             (nreverse
                                                               (split-string folder "/")))))
                                              (funcall name-fn (cdr (nreverse
                                                                      (split-string folder
                                                                                    "/")))
                                                               nil)
                                            (if (and (stringp item)
                                                     (string-equal account-name item))
                                                hier-root
                                              (if (and (stringp item)
                                                       (string-equal hier-root item))
                                                  nil
                                                account-name)))))))

                (switch-to-buffer (hierarchy-tree-display
                                    folder-hierarchy
                                    (lambda (item _)
                                      (if (stringp item)
                                          (insert item)
                                        (funcall (hierarchy-labelfn-button
                                                   (lambda (item _)
                                                     (if-let* ((unseen (alist-get 'unseen item))
                                                               (check  (> unseen 0)))
                                                         (insert " ("
                                                                 (number-to-string unseen)
                                                                 ") "
                                                                 (alist-get 'name item))
                                                       (insert (concat " " (alist-get 'name item)))))
                                                   (lambda (item _)
                                                     (let* ((next   (alist-get 'uidnext item))
                                                            (size   (> (string-to-number next) 200))
                                                            (to-use (if (not size)
                                                                        next
                                                                      (read-number
                                                                        (concat
                                                                          "There are potentially "
                                                                          next
                                                                          " messages; how many would "
                                                                          "you like to fetch? ")
                                                                        200))))
                                                       (make-thread
                                                         #'(lambda ()
                                                             (when-let ((buff (imel--connect-or-reconnect
                                                                                (alist-get 'account item))))
                                                               (imap-fetch (concat
                                                                             next
                                                                             ":"
                                                                             (number-to-string
                                                                               (-
                                                                                 (string-to-number next)
                                                                                 to-use)))
                                                                           "(BODY FLAGS RFC822)"
                                                                           t
                                                                           nil
                                                                           buff))

                                                             (message "You clicked on: %s"
                                                                      (alist-get 'name item)))))))
                                                 item
                                                 0)))
                                    folder-buffer))

                (imel--folders-mode))))))))

(defun imel--connect-or-reconnect (account-key)
  (if-let ((account (assq account-key imel---accounts-info)))
      (if-let* ((account-info (cdr account))
                (account-auth (auth-source-search :host (alist-get 'imap-server
                                                                   account-info))))
          (progn
            ;; We can't reasonably assume that `imap-opened' or
            ;; `imap-open' won't freeze/hang and we can't try it and then
            ;; check in 3 seconds without pausing either freezing this
            ;; function for 3 seconds or saving everything below in a
            ;; function to run in either scenario; at which point…it's
            ;; just less convoluted and painful to just kill the conn. and
            ;; make a new one
            (when-let* ((imap-connection-buffer (imel--get-imap-buffer account-key))
                        (buffer-exists-p        (get-buffer imap-connection-buffer)))
              (with-current-buffer imap-connection-buffer
                (let (kill-buffer-hook kill-buffer-query-functions)
                  (kill-buffer))))
            (setq imel---account-keys-to-imap-buffers
                  (cons (cons account-key
                              (imap-open (alist-get 'imap-server account-info)
                                         (alist-get 'imap-port   account-info)
                                         (alist-get 'imap-stream account-info)
                                         (alist-get 'imap-auth   account-info)))
                        imel---account-keys-to-imap-buffers))

            (if-let ((conn-buffer (imel--get-imap-buffer account-key))
                     (hier-root   "Accounts"))
                (progn
                  (imap-authenticate (plist-get (car account-auth) :user)
                                     (funcall (plist-get (car account-auth) :secret))
                                     conn-buffer)

                  conn-buffer)
              (message "Unable to open connection to IMAP server %s!"
                       (alist-get 'imap-server account-info))))
        (message "Improper server credentials saved for %s: %s!"
                 account-key
                 (alist-get 'imap-server account-info)))
    (message "Account info. is not stored for %s!" account-key)

    nil))
;; (async-start (lambda ()
;;                (message "start!")

;;                (sleep-for 6)

;;                222)
;;              (lambda (result)
;;                (message "Async process done, result should be 222: %s" result)))

(provide 'imel)

;;; imel.el ends here
